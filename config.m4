dnl $Id$
dnl config.m4 for extension mycoroutine

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

PHP_ARG_WITH(mycoroutine, for mycoroutine support,
Make sure that the comment is aligned:
[  --with-mycoroutine             Include mycoroutine support])

dnl Otherwise use enable:

dnl PHP_ARG_ENABLE(mycoroutine, whether to enable mycoroutine support,
dnl Make sure that the comment is aligned:
dnl [  --enable-mycoroutine           Enable mycoroutine support])

if test "$PHP_MYCOROUTINE" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-mycoroutine -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/mycoroutine.h"  # you most likely want to change this
  dnl if test -r $PHP_MYCOROUTINE/$SEARCH_FOR; then # path given as parameter
  dnl   MYCOROUTINE_DIR=$PHP_MYCOROUTINE
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for mycoroutine files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       MYCOROUTINE_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$MYCOROUTINE_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the mycoroutine distribution])
  dnl fi

  dnl # --with-mycoroutine -> add include path
  dnl PHP_ADD_INCLUDE($MYCOROUTINE_DIR/include)

  dnl # --with-mycoroutine -> check for lib and symbol presence
  dnl LIBNAME=mycoroutine # you may want to change this
  dnl LIBSYMBOL=mycoroutine # you most likely want to change this 

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $MYCOROUTINE_DIR/$PHP_LIBDIR, MYCOROUTINE_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_MYCOROUTINELIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong mycoroutine lib version or lib not found])
  dnl ],[
  dnl   -L$MYCOROUTINE_DIR/$PHP_LIBDIR -lm
  dnl ])
  dnl
  dnl PHP_SUBST(MYCOROUTINE_SHARED_LIBADD)

	STUDY_ASM_DIR="thirdparty/boost/asm/"

	AC_ARG_ENABLE(debug,
		[--enable-debug,  compile with debug symbols],
		[PHP_DEBUG=$enableval],
		[PHP_DEBUG=0]
	)

	AS_CASE([$host_os],
      [linux*], [STUDY_OS="LINUX"],
      []
    )

	AS_CASE([$host_cpu],
      [x86_64*], [STUDY_CPU="x86_64"],
      [x86*], [STUDY_CPU="x86"],
      [i?86*], [STUDY_CPU="x86"],
      [arm*], [STUDY_CPU="arm"],
      [aarch64*], [STUDY_CPU="arm64"],
      [arm64*], [STUDY_CPU="arm64"],
      []
    )

	if test "$STUDY_CPU" = "x86_64"; then
        if test "$STUDY_OS" = "LINUX"; then
            STUDY_CONTEXT_ASM_FILE="x86_64_sysv_elf_gas.S"
        fi
    elif test "$STUDY_CPU" = "x86"; then
        if test "$STUDY_OS" = "LINUX"; then
            STUDY_CONTEXT_ASM_FILE="i386_sysv_elf_gas.S"
        fi
    elif test "$STUDY_CPU" = "arm"; then
        if test "$STUDY_OS" = "LINUX"; then
            STUDY_CONTEXT_ASM_FILE="arm_aapcs_elf_gas.S"
        fi
    elif test "$STUDY_CPU" = "arm64"; then
        if test "$STUDY_OS" = "LINUX"; then
            STUDY_CONTEXT_ASM_FILE="arm64_aapcs_elf_gas.S"
        fi
    elif test "$STUDY_CPU" = "mips32"; then
        if test "$STUDY_OS" = "LINUX"; then
           STUDY_CONTEXT_ASM_FILE="mips32_o32_elf_gas.S"
        fi
    fi

	mycoroutine_source_file="\
        mycoroutine.c \
        src/coroutine/coroutine.c \
        src/coroutine/context.c \
        src/base/base.c \
        mycoroutine_coroutine.c \
        mycoroutine_coroutine_util.c \
        ${STUDY_ASM_DIR}make_${STUDY_CONTEXT_ASM_FILE} \
        ${STUDY_ASM_DIR}jump_${STUDY_CONTEXT_ASM_FILE}
    "

  PHP_NEW_EXTENSION(mycoroutine, $mycoroutine_source_file, $ext_shared,, -DZEND_ENABLE_STATIC_TSRMLS_CACHE=1)

	PHP_ADD_INCLUDE([$ext_srcdir])
    PHP_ADD_INCLUDE([$ext_srcdir/include])

	PHP_ADD_BUILD_DIR($ext_builddir/thirdparty/boost)
    PHP_ADD_BUILD_DIR($ext_builddir/thirdparty/boost/asm)
	PHP_ADD_BUILD_DIR($ext_builddir/src/coroutine)
	PHP_ADD_BUILD_DIR($ext_builddir/src/base)
fi

# mycoroutine

#### 介绍
一个用C语言实现的php协程扩展。

#### 软件架构
1. include包含头文件，其中mycoroutine.h包含标准库函数头文件
2. src，thirdparty包含各种c栈源文件
3. mycoroutine_coroutine_util.c 包含提供给php接口使用的代码
4. mycoroutine_coroutine.c 包含php栈协程相关

#### 安装教程

1.  /pathtophp/phpize
2.  ./configure --with-php-config=/pathtophp-config
3.  make && make install

#### 使用说明

![输入图片说明](https://images.gitee.com/uploads/images/2022/0614/173120_ca1a51ec_1927662.png "协程效果.png")



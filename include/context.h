#ifndef CONTEXT_H
#define CONTEXT_H

#include "asm_context.h"

typedef fcontext_t coroutine_context_t;
typedef void (*coroutine_func_t)(void*);

typedef struct _Context Context;
struct _Context{
	char *stack_;
	coroutine_func_t fn_;
	void *private_data_;
	uint32_t stack_size_;
	coroutine_context_t ctx_;
	coroutine_context_t swap_ctx_;
	bool end_;

	int (*swap_out)(Context *);
	int (*swap_in)(Context *);
};

Context *context_init(coroutine_func_t fn,size_t stack_size,void *private_data);
void context_dest(Context *ctx);
static inline int context_swap_out(Context *ctx);
static inline int context_swap_in(Context *ctx);
static inline void context_func(void *arg);

#endif

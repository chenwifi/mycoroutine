#ifndef BASE_H
#define BASE_H
#include "coroutine.h"

#define CORO_SIZE 100

typedef struct{
	Coroutine **coroutines;
	long corosize;
} coroArray;

typedef struct{
	Coroutine *current;
	long last_cid;
	coroArray coroutines;
	coroArray user_yield_coros;

	void (*on_yield)(void *);
	void (*on_resume)(void *);
} mcCoroutineG;
mcCoroutineG coroG;

void cg_init();
void cg_dest();
void Coroutine_array_init(coroArray *ca);
long Coroutine_array_push(Coroutine *co);
void Coroutine_array_insert(long cid,Coroutine *coro);
Coroutine* Coroutine_array_find(coroArray *coa,long cid);
void Coroutine_array_erase(Coroutine **cos,long cid);
Coroutine *get_current();

#endif

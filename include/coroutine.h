#ifndef COROUTINE_H
#define COROUTINE_H

#include "context.h"
#include "mycoroutine.h"

#define DEFAULT_C_STACK_SIZE          (2 *1024 * 1024)

typedef struct _Coroutine Coroutine;
struct _Coroutine{
	void *task;
	Context *ctx;
	Coroutine *origin;
	long cid;
	size_t stack_size;

	void* (*get_task)(Coroutine *);
	void (*on_close)(void *);
};

void* coroutine_get_current_task();
void* coroutine_get_task(Coroutine *coroutine);
long coroutine_create(coroutine_func_t fn,void *private_data);
Coroutine *coroutine_init(coroutine_func_t fn,void *private_data);
void coroutine_dest(Coroutine *coro);
long coroutine_run(Coroutine *coro);
void set_task(Coroutine * coro,void *_task);
void coroutine_check_end(Coroutine *coro);
void coroutine_close(Coroutine *coro);
Coroutine* coroutine_get_origin(Coroutine *coro);
long coroutine_get_cid(Coroutine *co);
void coroutine_yield(Coroutine *co);
void coroutine_resume(Coroutine *co);

#endif

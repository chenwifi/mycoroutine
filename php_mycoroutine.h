/*
  +----------------------------------------------------------------------+
  | PHP Version 7                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2016 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifndef PHP_MYCOROUTINE_H
#define PHP_MYCOROUTINE_H

#include "php.h"
#include "php_ini.h"
#include "php_globals.h"
#include "php_main.h"

#include "php_streams.h"
#include "php_network.h"

#include "zend_interfaces.h"
#include "zend_exceptions.h"
#include "zend_variables.h"
#include <ext/date/php_date.h>
#include <ext/standard/url.h>
#include <ext/standard/info.h>
#include <ext/standard/php_array.h>
#include <ext/standard/basic_functions.h>
#include <ext/standard/php_http.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef SW_STATIC_COMPILATION
#include "php_config.h"
#endif

#include "mycoroutine.h"

extern zend_module_entry mycoroutine_module_entry;
#define phpext_mycoroutine_ptr &mycoroutine_module_entry

#define PHP_MYCOROUTINE_VERSION "0.1.0" /* Replace with version number for your extension */

#ifdef PHP_WIN32
#	define PHP_MYCOROUTINE_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define PHP_MYCOROUTINE_API __attribute__ ((visibility("default")))
#else
#	define PHP_MYCOROUTINE_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

/*
  	Declare any global variables you may need between the BEGIN
	and END macros here:

ZEND_BEGIN_MODULE_GLOBALS(mycoroutine)
	zend_long  global_value;
	char *global_string;
ZEND_END_MODULE_GLOBALS(mycoroutine)
*/

/* Always refer to the globals in your function as MYCOROUTINE_G(variable).
   You are encouraged to rename these macros something shorter, see
   examples in any other php module directory.
*/
#define MYCOROUTINE_G(v) ZEND_MODULE_GLOBALS_ACCESSOR(mycoroutine, v)

#if defined(ZTS) && defined(COMPILE_DL_MYCOROUTINE)
ZEND_TSRMLS_CACHE_EXTERN()
#endif

#endif	/* PHP_MYCOROUTINE_H */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
